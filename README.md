# MTrack3

A motion tracker plugin for ImageJ with collision detection

Uses ImageJ's particle analyzer to track the movement of
multiple objects through a stack.

Based on the Object Tracker plugin filter by Wayne Rasband

Based on Multitracker, but should be quite a bit more intelligent

[Nico Stuurman, Vale Lab, UCSF/HHMI, May, June 2003]

Modified by David Eccles (gringer) <bioinformatics@gringene.org>

[David Eccles, MPI Münster, September 2011]

- new result save format, 7-column: track,frame,x,velX,y,velY,flags
- separate summary table: track,length,distance,nFrames,mean/median velocity
- can take previous velocity into account when discovering new positions
- particle processing changed to per frame, rather than per track
- collision is only flagged when a track cannot be assigned uniquely

## Installation

Download `MTrack3_/MTrack3_.java` and save to your ImageJ plugins folder,
then compile with the "Compile and Run" command. Restart ImageJ to add the
"MTrack3" command to the Plugins menu.

## Helper Scripts

Helper scripts (ImageJ Macros and R code) that may be of use can be found
in the `helper` directory.
