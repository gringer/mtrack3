#!/usr/bin/Rscript

## change variables here to affect script outcome
frame.co <- 33; # tracks shorter than this length will be ignored
mov.length <- 5; # length of image sequence in seconds
min.spd <- 1; # minimum mean track speed (in pixels per second)
default.fps <- 10; # number of frames per second

##change working directory
if(file.exists("/Users/gsteffe/Desktop/Tester Exports for tracking/")){
  setwd("/Users/gsteffe/Desktop/Tester Exports for tracking/");
}

input.files <- "";

## process command-line arguments (if any)
## command-line processing
argPos <- 1;
if(length(commandArgs(TRUE)) > 0){
  ## this makes sure that there is no default input file if command
  ## line arguments are used
  input.files <- NULL;
  output.directory <- "./";
}
while(argPos <= length(commandArgs(TRUE))){
  argument <- commandArgs(TRUE)[argPos];
  if(file.exists(argument)){
    input.files[length(input.files)+1] <- argument;
  }
  else {
    cat(sprintf("Command not understood: %s\n", argument));
  }
  argPos <- argPos + 1;
}

find.regions <- function(indata.df, min.frames, min.speed){
  indata.df <- subset(indata.df, !is.na(speed));
  ## Work out ranges for window region calculations
  track.stats <- data.frame(track = tapply(indata.df$RunTrack,indata.df$RunTrack,function(x){x[1]}),
                            minX = tapply(indata.df$X,indata.df$RunTrack,min),
                            maxX = tapply(indata.df$X,indata.df$RunTrack,max),
                            frames = tapply(indata.df$RunTrack, indata.df$RunTrack, length));
  track.stats <- track.stats[order(track.stats$track),];
  ## calculate additional track statistics based on inter-frame velocities
  track.stats$length <- tapply(indata.df$speed,indata.df$RunTrack,length);
  track.stats$meanVel <- tapply(indata.df$speed,indata.df$RunTrack,mean);
  track.stats$medVel <- tapply(indata.df$speed,indata.df$RunTrack,median);
  track.stats$SD <- tapply(indata.df$speed,indata.df$RunTrack,sd);
  track.stats$SE <- track.stats$SD / track.stats$length;
  ## limit to long tracks
  ## cat("Limiting to greater than", min.frames,"frames\n");
  track.stats <- subset(track.stats, frames >= min.frames);
  if(!is.null(min.speed)){
    track.stats <- subset(track.stats, medVel >= min.speed);
  }
  if(dim(track.stats)[1] == 0){
    warning("No tracks to process (was the minimum track length set too high?)");
    return(NULL);
  }

  ## calculate window regions
  track.stats <- track.stats[order(track.stats$minX),];
  region.stats <- data.frame(region = integer(0), minX = numeric(0), maxX = numeric(0));
  current.region <- track.stats[1,c("minX","maxX")];
  region.id <- 1;
  for(current.track in 1:(dim(track.stats)[1])){
    if(all(sign(track.stats[current.track,c("minX","maxX")] - rev(current.region)) == c(-1,1)) ||
       all(sign(track.stats[current.track,c("minX","maxX")] - rev(current.region)) == c(0,0))){
      current.region["minX"] <- min(current.region["minX"],track.stats$minX[current.track]); 
      current.region["maxX"] <- max(current.region["maxX"],track.stats$maxX[current.track]);
      track.stats$region[current.track] <- region.id;
    } else {
      region.stats[region.id,] <-
        data.frame(region = region.id, minX = current.region["minX"], maxX = current.region["maxX"]);
      current.region <- track.stats[current.track,c("minX","maxX")];
      region.id <- region.id + 1;
      track.stats$region[current.track] <- region.id;
    }
  }
  region.stats[region.id,] <-
    data.frame(region = region.id, minX = current.region["minX"], maxX = current.region["maxX"]);
  rownames(region.stats) <- region.stats$region;
  region.stats$meanSpd <- tapply(track.stats$meanVel, track.stats$region, mean);
  region.stats$sdSpd <- tapply(track.stats$meanVel, track.stats$region, sd);
  region.stats$seSpd <-  region.stats$sdSpd / tapply(track.stats$meanVel, track.stats$region, length);
  region.stats$meanFrames <- tapply(track.stats$frames, track.stats$region, mean);
  region.stats$minSpd <- tapply(track.stats$meanVel, track.stats$region, min);
  region.stats$maxSpd <- tapply(track.stats$meanVel, track.stats$region, max);
  region.stats$cv <-  region.stats$sdSpd / region.stats$meanSpd;
  return(region.stats);
}

process.positions <- function(file.name = NULL, run.number = NULL, repeat.code = NULL,
                              replicate = NULL, start.frame = NULL, movie.length = NULL,
                              frames.per.second = NULL, frame.cutoff = NULL,
                              min.speed = NULL, result.format = NULL){
  base.name <- "";
  subdir <- ".";
  data.df <- NULL;
  if(is.null(file.name)){
    base.name <- paste(sprintf("%02d",run.number), repeat.code, replicate, "+", start.frame, sep = "");
    subdir <- paste("Run",sprintf("%03d",run.number),toupper(repeat.code),replicate,"at",frames.per.second,"fps",sep="");
    data.df <- process.file(run.number, repeat.code, replicate,
                            start.frame, movie.length, frames.per.second,
                            frame.cutoff, min.speed, result.format);
  } else {
    data.df <- process.file(input.filename = file.name, run.number = run.number, repeat.code = repeat.code, replicate = replicate, start.frame = start.frame, movie.length = movie.length, frames.per.second = frames.per.second, frame.cutoff = frame.cutoff, min.speed = min.speed, result.format = result.format);
    dirSepPos <- gregexpr("/",file.name)[[1]];
    if(max(dirSepPos) > -1){
      subdir <- substring(file.name, 1, max(dirSepPos)-1);
      file.name <- substring(file.name, max(dirSepPos)+1);
    }
    base.name <- sub("^positions_","",sub("\\.csv$","",file.name));
    if(is.null(frames.per.second)){
      fps.pos <- regexpr("[0-9]+fps",base.name);
      fps.text.pos <- regexpr("fps",base.name)-1;
      if(fps.pos != -1){
        frames.per.second = as.numeric(substring(base.name,fps.pos,fps.text.pos));
      } else {
        frames.per.second = 10; # default to 10fps
      }
    }
    if(is.null(run.number)){
      run.pos <- regexpr("^(Run)?[0-9]+[a-zA-Z]",base.name);
      if(run.pos != -1){
        run.number <- as.numeric(substring(base.name,run.pos+3,run.pos+attr(run.pos,"match.length")-2));
        repeat.code <- substring(substring(base.name,run.pos+attr(run.pos,"match.length")-1),1,1);
        replicate <- as.numeric(substring(substring(base.name,run.pos+attr(run.pos,"match.length")-1),2,2));
      } else if(is.null(run.number)){
        run.number <- 1;
        repeat.code <- "X";
        replicate <- 1;
      }
    }
  }
  ## Work out ranges for window region calculations
  track.stats <- data.frame(region = NA,
                            track = tapply(data.df$RunTrack,data.df$RunTrack,function(x){x[1]}),
                            minX = tapply(data.df$X,data.df$RunTrack,min),
                            maxX = tapply(data.df$X,data.df$RunTrack,max),
                            frames = tapply(data.df$RunTrack, data.df$RunTrack, length));

  ## calculate additional track statistics based on inter-frame velocities
  track.stats$length <- tapply(data.df$speed,data.df$RunTrack,function(x){length(na.omit(x))});
  track.stats$meanVel <- tapply(data.df$speed,data.df$RunTrack,mean, na.rm = TRUE);
  track.stats$medVel <- tapply(data.df$speed,data.df$RunTrack,median, na.rm = TRUE);
  track.stats$SD <- tapply(data.df$speed,data.df$RunTrack,sd, na.rm = TRUE);
  track.stats$SE <- track.stats$SD / track.stats$length;

  track.stats <- track.stats[order(track.stats$track),];
  ## calculate additional track statistics based on inter-frame velocities
  cat("Writing run histogram to ",paste(subdir,"/inter-frame_speeds_",base.name,".pdf",sep=""),"...",sep="");
  pdf(paste(subdir,"/inter-frame_speeds_",base.name,".pdf",sep=""), paper = "a4r", width = 11, height = 8);
  hist(data.df$speed,
       main = paste("Inter-frame speeds for",base.name),
       xlab = "speed (pixels/s)", breaks = 20, col = "red");
  dummy <- dev.off();
  cat(" done\n");
  ## limit to long tracks
  cat("Limiting to at least", frame.cutoff,"frames\n");
  track.stats <- subset(track.stats, frames >= frame.cutoff);
  if(!is.null(min.speed)){
    track.stats <- subset(track.stats, medVel >= min.speed);
  }
  if(dim(track.stats)[1] == 0){
    warning("No tracks to process (was the minimum track length set too high?)");
    return(NULL);
  }

  ## calculate window regions
  track.stats <- track.stats[order(track.stats$minX),];
  region.stats <- data.frame(region = integer(0), minX = numeric(0), maxX = numeric(0));
  current.region <- track.stats[1,c("minX","maxX")];
  region.id <- 1;
  for(current.track in 1:(dim(track.stats)[1])){
    if(all(sign(track.stats[current.track,c("minX","maxX")] - rev(current.region)) == c(-1,1)) ||
       all(sign(track.stats[current.track,c("minX","maxX")] - rev(current.region)) == c(0,0))){
      current.region["minX"] <- min(current.region["minX"],track.stats$minX[current.track]);
      current.region["maxX"] <- max(current.region["maxX"],track.stats$maxX[current.track]);
      track.stats$region[current.track] <- region.id;
    } else {
      region.stats[region.id,] <-
        data.frame(region = region.id, minX = current.region["minX"], maxX = current.region["maxX"]);
      current.region <- track.stats[current.track,c("minX","maxX")];
      region.id <- region.id + 1;
      track.stats$region[current.track] <- region.id;
    }
  }
  region.stats[region.id,] <-
    data.frame(region = region.id, minX = current.region["minX"], maxX = current.region["maxX"]);
  rownames(region.stats) <- region.stats$region;
  ## output to file
  track.stats <- track.stats[order(track.stats$track),];
  track.stats <- track.stats[order(track.stats$region),];
  cat("Writing track summaries to ",paste(subdir,"/tracks_",base.name,".csv",sep=""),"...",sep="");
  if(file.exists(paste(subdir,"/tracks_",base.name,".csv",sep=""))){
    cat(" warning: file already exists, this will be overwritten...");
  }
  write.csv(track.stats, file = paste(subdir,"/tracks_",base.name,".csv",sep=""), row.names = FALSE);
  cat(" done\n");

  ## calculate additional per-region statistics
  ## note: because this mean is a mean-of-means, the SD should already account for
  ##       sample size (http://www.westgard.com/lesson35.htm#7)
  region.stats$meanVel <- tapply(track.stats$meanVel, track.stats$region, mean);
  region.stats$sd <- tapply(track.stats$meanVel, track.stats$region, sd);
  region.stats$se <-  region.stats$sd / tapply(track.stats$meanVel, track.stats$region, length);
  region.stats$meanFrames <- tapply(track.stats$frames, track.stats$region, mean);
  region.stats$minSpd <- tapply(track.stats$meanVel, track.stats$region, min);
  region.stats$maxSpd <- tapply(track.stats$meanVel, track.stats$region, max);
  region.stats$cv <-  region.stats$sd / region.stats$meanVel;

  ## output to file
  cat("Writing region summaries to ",paste(subdir,"/regions_",base.name,".csv",sep=""),"...",sep="");
  if(file.exists(paste(subdir,"/regions_",base.name,".csv",sep=""))){
    cat(" warning: file already exists, this will be overwritten...");
  }
  write.csv(region.stats, file = paste(subdir,"/regions_",base.name,".csv",sep=""), row.names = FALSE);
  cat(" done\n");
}

process.file <- function(input.filename = NULL, run.number = NULL, repeat.code = NULL,
                         replicate = NULL, start.frame = NULL, movie.length = NULL,
                         frames.per.second = NULL,
                         frame.cutoff = NULL, min.speed = NULL, result.format = NULL){
  base.name <- "";
  subdir <- ".";
  if(is.null(input.filename)){
    num.frames <- movie.length * frames.per.second + 1;
    ## file format convention: <number><code><replicate>+<startFrame> e.g. 33a2+70
    base.name <- paste(sprintf("%02d",run.number), repeat.code, replicate, "+", start.frame, sep = "");
    subdir <- sprintf("Run%03d%s%dat%dfps",run.number,toupper(repeat.code),replicate,frames.per.second);
    input.filename <- list.files(subdir,pattern="^(positions_)?(Run)?[0-9]+[a-zA-Z][0-9]+(\\+[0-9]+|at10fps)(.csv|.txt)?");
    if(length(input.filename) == 0){
      stop(paste("no appropriate result file found in",subdir));
    }
    base.name <- input.filename[1];
    input.filename <- paste(subdir,"/",base.name, sep = "");
  } else {
    dirSepPos <- gregexpr("/",input.filename)[[1]];
    if(max(dirSepPos) > -1){
      subdir <- substring(input.filename, 1, max(dirSepPos)-1);
      input.filename <- substring(input.filename, max(dirSepPos)+1);
    }
    base.name <- sub("^positions_","",sub("\\.csv$","",input.filename));
    if(is.null(frames.per.second)){
      fps.pos <- regexpr("[0-9]+fps",base.name);
      fps.text.pos <- regexpr("fps",base.name)-1;
      if(fps.pos != -1){
        frames.per.second = as.numeric(substring(base.name,fps.pos,fps.text.pos));
      } else {
        frames.per.second = 10; # default to 10fps
      }
    }
    if(is.null(run.number)){
      run.pos <- regexpr("^(Run)?[0-9]+[a-zA-Z]",base.name);
      if(run.pos != -1){
        run.number = as.numeric(substring(base.name,run.pos+3,run.pos+attr(run.pos,"match.length")-2));
        repeat.code = substring(substring(base.name,run.pos+attr(run.pos,"match.length")-1),1,1);
        replicate = as.numeric(substring(substring(base.name,run.pos+attr(run.pos,"match.length")-1),2,2));
      }
    }
  }
  input.filename <- sprintf("%s/%s",subdir,input.filename);
  cat("Processing file ",input.filename, "\n", sep = "");

  data.df <- NULL;
  if(is.null(result.format)){
    firstLine <- readLines(input.filename, n = 1);
    if(grepl("velX",firstLine)){
      ## contains per-particle velocity, so consider to be Mtrack3 format
      result.format <- "MTrack3";
    } else {
      result.format <- "MTrack2";
    }
  }
  if(result.format == "MTrack3"){
    cat("Looks like an MTrack3 file\n");
    ## load file into data frame
    data.df <- read.csv(input.filename, row.names = 1);
    ## replace column names, because R thinks the first unlabelled column should be 'X'
    colnames(data.df) <- sub(".1","",colnames(data.df));
  } else if(result.format == "MTrack2"){
    cat("Looks like an MTrack2 file\n");
    input.file <- file(input.filename);
    open(input.file);
    ## get header line
    header.line <- scan(input.file, what = character(0), nlines = 1, quiet = TRUE);
    ## extract out track listing
    track.line <- unlist(strsplit(readLines(input.file, n = 1)," "));
    ## set up rotated results table
    results <- data.frame(run = NULL, Frame = NULL, Track = NULL, X = NULL, Y = NULL);
    while(length(track.line) == 4){
      ## read num.frames lines from the file
      data.df <- read.delim(input.file, nrows = num.frames, header = FALSE);
      ## determine track numbers for columns
      track.start = as.numeric(track.line[2]);
      track.end = as.numeric(track.line[4]);
      ## cat("Iteration for",track.start,"...",track.end,"\n");
      ## add data to end of results structure
      if(track.start < track.end){ # work around odd bug if exactly 75 tracks
        for(trackPos in (track.start:track.end - track.start)){
          added.df <- data.frame(run = paste(run.number, repeat.code, replicate),
                                 Frame = data.df[,1],
                                 Track = trackPos + track.start,
                                 X = data.df[,2+trackPos * 3],
                                 Y = data.df[,3+trackPos * 3]);
          if(length(na.omit(added.df$X)) > frame.cutoff){
            results <- rbind(results, added.df);
          }
        }
      }
      track.line <- unlist(strsplit(readLines(input.file, n = 1)," "));
    }
    close(input.file);
    results <- subset(results, !is.na(X));
    data.df <- results;
  }
  data.df <- data.df[order(data.df$Track),];
  data.df$speed <- unlist(tapply(1:length(data.df$Track),
             data.df$Track,function(x){c(NA,
               sqrt(diff(data.df$X[x])^2 + diff(data.df$Y[x])^2) * frames.per.second)
             }));
  data.df$RunTrack <- sprintf("%d%s%03d",run.number,repeat.code,data.df$Track);
  return(data.df);
}


## example: process a single run
## process.positions(run.number = 22, repeat.code = "a", replicate = 5,
##                  start.frame = 20, movie.length = 5, frames.per.second = 10,
##                  frame.cutoff = 33, min.speed = 5);

if((length(input.files) == 1) && (input.files == "")){
  ## process results in directories that look like e.g. Run008A1at10fps
  dirsToProcess <- list.files(pattern = "Run[0-9]{3}[a-zA-Z][0-9]at[0-9]+fps");
  for(x in dirsToProcess){
    fps.matchStart <- gregexpr("[0-9]+fps",x)[[1]];
    fps.matchLength <- attr(fps.matchStart,"match.length");
    fps.matchEnd <- fps.matchStart + fps.matchLength - 4;
    fps <- as.numeric(substring(x,fps.matchStart,fps.matchEnd));
    resultToProcess <- list.files(path = x, pattern = "^[0-9]+[a-zA-Z][0-9]+\\+[0-9]+");
    number.matchStart <- gregexpr("[0-9]+",resultToProcess)[[1]];
    number.matchLength <- attr(number.matchStart,"match.length");
    number.matchEnd <- number.matchStart + number.matchLength - 1;
    number.matches <- as.numeric(substring(resultToProcess, number.matchStart, number.matchEnd));
    text.matchStart <- gregexpr("[a-zA-Z]+",resultToProcess)[[1]];
    text.matchLength <- attr(text.matchStart,"match.length");
    text.matchEnd <- text.matchStart + text.matchLength - 1;
    text.matches <- substring(resultToProcess, text.matchStart, text.matchEnd);
    run.num <- number.matches[1];
    rept.code <- text.matches[1];
    repl <- number.matches[2];
    start.f <- number.matches[3];
    ## file format convention: <number><code><replicate>+<startFrame> e.g. 33a2+70
    process.positions(run.number = run.num, repeat.code = rept.code, replicate = repl,
                      start.frame = start.f, movie.length = mov.length, frames.per.second = fps,
                      frame.cutoff = frame.co, min.speed = min.spd);
  }
} else {
  for(fileName in input.files){
    dirSepPos <- gregexpr("/",fileName)[[1]];
    base.name <- fileName;
    if(max(dirSepPos) > -1){
      base.name <- substring(fileName, max(dirSepPos)+1);
    }
    if(grepl("DSC",fileName)){
      run.pos <- regexpr("[0-9]+", base.name);
      run.number <- as.numeric(substring(base.name,run.pos,run.pos+attr(run.pos,"match.length")-1));
      process.positions(fileName, frame.cutoff = frame.co, min.speed = min.spd, movie.length = mov.length,
                        run.number = run.number, repeat.code = "DSC", replicate = 0);
    } else {
      process.positions(fileName, frame.cutoff = frame.co, min.speed = min.spd, movie.length = mov.length);
    }
  }
}


## ## Data table for meeting
## run.num = 22;
## data.df <- data22a.df;
## for (run.rep in c(1:5,0)){
##   if(run.rep == 0){
##     tmpdata.df <- subset(data.df, !is.na(speed));
##   } else {
##     tmpdata.df <- subset(data.df, !is.na(speed) & (run == paste(run.num, "a", run.rep)));
##   }
##   cat(paste(c(length(tmpdata.df$speed),
##               mean(tmpdata.df$speed),
##               sd(tmpdata.df$speed),
##               sd(tmpdata.df$speed) / length(tmpdata.df$speed),
##               min(tmpdata.df$speed),
##               max(tmpdata.df$speed),
##               sd(tmpdata.df$speed) / mean(tmpdata.df$speed)),collapse=","),"\n", sep = "");
## }
## for (run.rep in c(1:5,0)){
##   if(run.rep == 0){
##     tmpdata.df <- subset(data.df, !is.na(speed));
##   } else {
##     tmpdata.df <- subset(data.df, !is.na(speed) & (run == paste(run.num, "a", run.rep)));
##   }
##   cat(paste(c(length(tapply(tmpdata.df$speed,tmpdata.df$RunTrack,mean)),
##               mean(tapply(tmpdata.df$speed,tmpdata.df$RunTrack,mean)),
##               sd(tapply(tmpdata.df$speed,tmpdata.df$RunTrack,mean)),
##               sd(tapply(tmpdata.df$speed,tmpdata.df$RunTrack,mean))
##               / length(tapply(tmpdata.df$speed,tmpdata.df$RunTrack,mean)),
##               min(tapply(tmpdata.df$speed,tmpdata.df$RunTrack,mean)),
##               max(tapply(tmpdata.df$speed,tmpdata.df$RunTrack,mean)),
##               sd(tapply(tmpdata.df$speed,tmpdata.df$RunTrack,mean))
##               / mean(tapply(tmpdata.df$speed,tmpdata.df$RunTrack,mean))),collapse=","),"\n", sep = "");
## }
## allregions.df <- NULL;
## for (run.rep in c(1:5,0)){
##   if(run.rep != 0){
##     tmpdata.df <- subset(data.df, !is.na(speed) & (run == paste(run.num, "a", run.rep)));
##     regions.df <- find.regions(tmpdata.df, min.frames = 33, min.speed = 6);
##     allregions.df <- rbind(cbind(rep = run.rep,regions.df), allregions.df);
##   } else {
##     regions.df <- allregions.df;
##   }
##   cat(paste(c(length(regions.df$meanSpd),
##               mean(regions.df$meanSpd),
##               sd(regions.df$meanSpd),
##               sd(regions.df$meanSpd) / length(regions.df$meanSpd),
##               min(regions.df$meanSpd),
##               max(regions.df$meanSpd),
##               sd(regions.df$meanSpd) / mean(regions.df$meanSpd)),
##             collapse=","),"\n", sep = "");
## }
