#!/usr/bin/Rscript
## if run == e.g. 8A, then will pull out results for A1,A2,A3,A4,A5
## if run == e.g. 8A1, then will only pull out results for 8A1
## set run / regions to select
file.base <- "testDSC secondary";
## set bins for histogram (10,12,...,58,60)
hist.bins <- seq(0, 80, by = 2); 

##change working directory
if(file.exists("/Users/gsteffe/Desktop/MP new device/MP 05 December 2011 copy 2/")){
  setwd("/Users/gsteffe/Desktop/MP new device/MP 05 December 2011 copy 2/");
}

#process.dirs <- function(){
#  run.regions <- list("Run008" = 1:10, "Run034" = 1:10, "Run035" = 1:10,
#                      "Run037" = 1:10);
#  alldata.df <- NULL;
#  for(run.name in names(run.regions)){
#    data.df <- NULL;
#    ## find matching subdirectories
#    dirs <- list.files(pattern = paste(run.name,".*at10fps",sep=""));
#    run.count <- length(dirs);
#    print(dirs); # show directories found that match this pattern
#    ## get and combine data from matching subdirectories
#    for(dirName in dirs){
#    next.name <- list.files(path = dirName, pattern = "tracks_.*.csv",
#                            full.names=TRUE)[1];
#    base.name <- sub("\\.csv","",sub("^.*/","",next.name));
#    next.data <- data.frame(run = base.name, read.csv(next.name));
#    data.df <- rbind(data.df, next.data);
#  }
#    data.df <- subset(data.df, region %in% run.regions[[run.name]]);
#    alldata.df <- rbind(alldata.df, data.df);
#  }
#  return(alldata.df);
#}

##Specify Genotypes for Runs
process.files <- function(){
  file.names <- list("Shi12-12B/w1118;+/+;nsybG4/+_Run092A" = c("4619","4620","4621","4622","4623")

 
                     
                     );
  alldata.df <- NULL;
  for(genotype in names(file.names)){
    for(file.name in file.names[[genotype]]){
      next.name <- list.files(pattern=sprintf("tracks_(Run|DSC_)?%s",file.name), ignore.case = TRUE)[1];
      cat("Processing",next.name," (from",file.name,")\n");
      next.data <- data.frame(genotype = genotype, run = next.name, read.csv(next.name));
      alldata.df <- rbind(alldata.df, next.data);
    }
  }
  return(alldata.df);
}

alldata.df <- process.files();

alldata.df$base.run <- sub("tracks_","",sub("[ABC].*$","",alldata.df$run));
alldata.df$full.run <- sub("tracks_","",sub("at.*$","",alldata.df$run));
alldata.df$run.region <- sprintf("%s-%s-%02d",alldata.df$full.run,alldata.df$genotype,alldata.df$region);
write.csv(alldata.df, file = sprintf("%s_alldata.csv", file.base), row.names = FALSE);

regions.df <-
  data.frame(genotype = tapply(alldata.df$genotype,alldata.df$run.region,function(x){as.character(x[1])}),
             run.region = tapply(alldata.df$run.region,alldata.df$run.region,function(x){x[1]}),
             tracks = tapply(alldata.df$meanVel,alldata.df$run.region,length),
             meanSpeed = tapply(alldata.df$meanVel,alldata.df$run.region,mean),
             SD=tapply (alldata.df$meanVel,alldata.df$run.region,sd))
write.csv(regions.df, file = sprintf("%s_regionmeans.csv", file.base), row.names = FALSE);

## Calculate means by tracks shared by the same genotype
tracksum.df <-
data.frame (AccuMeanVel=tapply(alldata.df$meanVel,alldata.df$genotype,mean),
			SD=tapply (alldata.df$meanVel,alldata.df$genotype,sd),
			track_count=tapply (alldata.df$genotype,alldata.df$genotype,length))
tracksum.df$Ratio <- tracksum.df$AccuMeanVel / tracksum.df$AccuMeanVel["w1118/w1118;+/+;nsybG4/+ Run092A"]			
write.csv(tracksum.df, file = sprintf("%s_tracksummeans.csv", file.base), row.names = TRUE)

##Calculated normalized distributions for tracks sharing the same genotype
Densitybygenotype.df <-
data.frame (row.names =hist.bins[-1])
for(x in unique(regions.df$genotype)){
	subset.df <- (subset)(alldata.df, genotype==x)
	out.hist <- hist(subset.df$meanVel, breaks = hist.bins, plot = FALSE);
	Densitybygenotype.df[,x]<-(out.hist$density)
}
write.csv(Densitybygenotype.df, file = sprintf ("%s_Densitybygenotype.csv", file.base), row.names = TRUE)

regions.df <- subset(regions.df, tracks >= 3);

test.df <- data.frame(x = NULL, y = NULL, mean.x = NULL, mean.y = NULL,
                      t = NULL, df = NULL, p.value = NULL);
for(x in unique(regions.df$genotype)){
  for(y in unique(regions.df$genotype)){
    result <-
      t.test(subset(regions.df, genotype == x)$meanSpeed,
             subset(regions.df, genotype == y)$meanSpeed,
             paired = FALSE);
    test.df <-
      rbind(test.df, data.frame(row.names = sprintf("%s-%s", x, y), x = x, y = y,
                                mean.x = result$estimate[1],
                                mean.y = result$estimate[2],
                                t = result$statistic,
                                df = result$parameter,
                                p.value = round(result$p.value,6)));
  }
}
write.csv(test.df, file = sprintf("%s_ttest.csv", file.base), row.names = FALSE);


## out.hist <- hist(alldata.df$meanVel, breaks = hist.bins, prob = TRUE);
## data.hist <- data.frame(bin = sprintf("%s-%s",out.hist$breaks[-21], out.hist$breaks[-1]),
##                         counts = out.hist$counts,
##                         density = out.hist$density,
##                         normalised = out.hist$density * 4);
## write.csv(data.hist, file = sprintf("hist_%s.csv", file.base), row.names = FALSE);

## pdf(sprintf("speeds_%s.pdf",file.base), paper = "a4r",
##     width = 11, height = 8);
## par(mar = c(5.1,5.1,0,0));
## ## histogram of all found mean track speeds
## hist.data <-
##   hist(alldata.df$meanVel, freq = FALSE,
##        col = "red", breaks = hist.bins, main = "",
##        xlab = sprintf("Mean track fly speeds (%s)",run.name),
##        cex.axis = 1.5, cex.lab = 2);
## ## hist.data$density is normalised to the total number of tracks in
## ## the run
## print(sum(hist.data$density));
## dummy <- dev.off();
